namespace Ex31_ObserverPattern;

public class ConcreteObserver : Observer
{
    private ConcreteSubject _subject;
    private int state;
    
    public int State
    {
        get { return state; }
        set { state = value; }
    }

    public ConcreteObserver(ConcreteSubject subject)
    {
        _subject = subject;
        state = subject.State;
    }
    
    public override void Update()
    {
        state = _subject.State;
    }
}